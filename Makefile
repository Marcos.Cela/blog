PHONY : build push local help
.DEFAULT_GOAL := help

DOCKER_IMAGE := registry.gitlab.com/marcos.cela/wiki:latest


help: ## Shows this help message and all the other targets with nice colored output
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build docker image
	@docker build -t $(DOCKER_IMAGE) .

push: build ## Build & push docker image
	@docker push $(DOCKER_IMAGE)


local: ## Runs hugo server on local mode
	@docker run -it  --name hugo --rm -p 1313:1313 -e HUGO_WATCH=true -e HUGO_BASEURL="http://localhost" -v $$(pwd)/site:/src jojomi/hugo


