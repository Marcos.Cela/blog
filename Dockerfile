FROM alpine:3.7 as builder
ENV HUGO_VERSION 0.54.0
ENV HUGO_BINARY hugo_${HUGO_VERSION}_linux-64bit
ENV HUGO_ENV "production"

# Download and Install hugo
RUN mkdir /usr/local/hugo \
    && \
    wget -O  /usr/local/hugo/${HUGO_BINARY}.tar.gz \
    https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY}.tar.gz \
    && \
    tar xzf /usr/local/hugo/${HUGO_BINARY}.tar.gz -C /usr/local/hugo/ \
	&& ln -s /usr/local/hugo/hugo /usr/local/bin/hugo \
	&& rm /usr/local/hugo/${HUGO_BINARY}.tar.gz
# Build the site
ADD site /site
RUN cd /site && \
    hugo --minify --i18n-warnings --buildFuture

FROM nginx:stable-alpine as nginx
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /site/public /usr/share/nginx/html
