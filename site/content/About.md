---
title: About
description: What is this page, who am I and where is my TAB?
type: page
featured_image: "/images/lugo.jpg"
menu:
  main: {}

---

{{< figure src="/images/marcos.jpg" title="Me enjoying some sun!" >}}

I am Marcos, a Telecommunications engineer specialized in telematics. Currently loving all the things
 that have to do with:
 
 * ***Kubernetes***
 * ***DevOps***
 * ***Docker***
 * ***Monitoring***
 * ***Automation***
 * ***Java***
 * ***Go***
 * ***Python***

 